help: ## Помощь
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

up: ## Старт контейнера
	docker compose up -d
down: ## Остановка контейнера
	docker compose down

build: ## Сборка проекта
	cp .env.example .env
	docker compose build
	docker compose up -d
	#docker compose exec php-fpm composer install
	#docker compose exec php-fpm bin/console doctrine:migrations:migrate

up-bd:
	docker compose up -d postgres

bash:
	docker compose exec -i php-fpm bash

postgres:
	docker compose exec postgres psql -Uuser -hpostgres -dpostgres #password

node:
	docker compose exec -i node bash

#./bin/console doctrine:schema:validate
#./bin/console doctrine:migrations:diff
#./bin/console doctrine:migrations:migrate
#./bin/console d:d:c --env=test // создание bd
#./bin/console d:m:m --env=test // миграция
#./vendor/bin/codecept run unit
#./vendor/bin/codecept run functional
#./vendor/bin/codecept run acceptance
#vendor/bin/codecept run clean
#./vendor/bin/codecept build // пересборка тестера
#./bin/console doctrine:schema:update --force
#./bin/console doctrine:fixtures:load
#./bin/console participant:clear-send
#./bin/console messenger:consume async
#./bin/console cache:warmup //прогрев кеша