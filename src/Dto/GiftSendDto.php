<?php

declare(strict_types=1);

namespace App\Dto;

class GiftSendDto
{
    public function __construct(
        readonly private string $name,
        readonly private string $email,
        readonly private string $santaEmail,
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getSantaEmail(): string
    {
        return $this->santaEmail;
    }

}