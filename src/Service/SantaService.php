<?php

declare(strict_types=1);

namespace App\Service;

use App\Command\Santa\SendEmails\SendEmailsCommand;
use App\Command\Santa\Set\SetSantaCommand;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

class SantaService
{
    public function __construct(
        private readonly MessageBusInterface $commandBus,
    )
    {
    }

    public function assignSantas(): void
    {
        try {
            $this->commandBus->dispatch(new SetSantaCommand());
        } catch (HandlerFailedException $e) {
        }
    }

    public function send(): void
    {
        try {
            $this->commandBus->dispatch(new SendEmailsCommand());
        } catch (HandlerFailedException $e) {
        }
    }
}