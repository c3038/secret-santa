<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\GiftSendDto;
use App\Entity\Participant;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailService
{
    public function __construct(
        private readonly MailerInterface $mailer,
    )
    {
    }

    public function sendEmail(GiftSendDto $dto): bool
    {
        $email = (new Email())
            ->from($dto->getSantaEmail())
            ->to($dto->getEmail())
            ->subject('Поздравление с праздником')
            ->html(
                sprintf('Поздравляю с праздником %s', $dto->getName()).
                '<br> Отправитель'.
                $dto->getSantaEmail()
            )
            ;
        try {
            $this->mailer->send($email);

            return true;
        } catch (TransportExceptionInterface $e) {
            return false;
        }
    }
}