<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ParticipantRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

#[ORM\Entity(repositoryClass: ParticipantRepository::class)]
#[ORM\Table(name: 'participant')]
#[UniqueEntity('email', message: 'Участник с email {{ value }} уже существует.')]
#[ORM\Index(columns: ['email'], name: 'participant__email__ind')]
class Participant
{
    const STATUS_NOT_CONGRATULATED = false;

    #[ORM\Column(name: 'id', type: Types::INTEGER, unique: true)]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    private ?int $id = null;

    #[Assert\NotBlank(message: 'Поле {{ label }} не должно быть пустым')]
    #[ORM\Column(name: 'name', type: Types::STRING, length: 255,  nullable: false,)]
    private string $name;

    #[Assert\NotBlank(message: 'Поле {{ label }} не должно быть пустым')]
    #[Assert\Email(
        message: 'Поле {{ label }} {{ value }} не соответствует формату email.',
    )]
    #[ORM\Column(name: 'email', type: Types::TEXT, length: 255, nullable: false,)]
    private string $email;

    #[ORM\OneToOne(targetEntity: Participant::class, fetch: 'LAZY')]
    #[ORM\JoinColumn(name: 'santa', referencedColumnName: 'id')]
    private ?Participant $santa = null;

    #[ORM\Column(name: 'congratulated', type: Types::BOOLEAN, options: ["default" => self::STATUS_NOT_CONGRATULATED])]
    private ?bool $congratulated = self::STATUS_NOT_CONGRATULATED;

    #[ORM\Column(name: 'created_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'create')]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(name: 'updated_at', type: 'datetime_immutable', nullable: false)]
    #[Gedmo\Timestampable(on: 'update')]
    private \DateTimeImmutable $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getSanta(): ?Participant
    {
        return $this->santa;
    }

    public function setSanta(?Participant $santa): static
    {
        $this->santa = $santa;
        return $this;
    }

    public function getCongratulated(): ?bool
    {
        return $this->congratulated;
    }

    public function setCongratulated(?bool $congratulated): static
    {
        $this->congratulated = $congratulated;
        return $this;
    }

    public function __toString(): string
    {
        return $this->email;
    }
}