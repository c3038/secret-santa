<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Participant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

class ParticipantRepository extends ServiceEntityRepository
{
    const DEFAULT_PER_PAGE = 4;

    public function __construct(ManagerRegistry $registry,)
    {
        parent::__construct($registry, Participant::class);
    }

    public function countById(): ?int
    {
        $qb = $this->createQueryBuilder('p')
            ->select('COUNT(p.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getAllWithSantaQuery(): Query
    {
        return $this->_em->createQueryBuilder()
            ->select('p', 's')
            ->from(Participant::class, 'p')
            ->leftJoin('p.santa', 's')
            ->orderBy('p.id', 'ASC')
            ->getQuery();
    }

    public function getAllQuery(): Query
    {
        return $this->_em->createQueryBuilder()
            ->select('p')
            ->from(Participant::class, 'p')
            ->orderBy('p.id', 'ASC')
            ->getQuery();
    }
    public function getWithSantaAll(): array
    {
        return $this->getAllWithSantaQuery()->getResult();
    }

    public function getAll(): array
    {
        return $this->getAllQuery()->getResult();
    }

    public function getAllWithRelativePagination(int $perPage = self::DEFAULT_PER_PAGE): iterable
    {
        $query = $this->getAllQuery();

        foreach ($this->paginationPreset($query, $perPage) as $paginate){
            yield $paginate;
        }
    }

    public function getAllWithSantaWithRelativePagination(int $perPage = self::DEFAULT_PER_PAGE): iterable
    {
        $query = $this->getAllWithSantaQuery();

        foreach ($this->paginationPreset($query, $perPage) as $paginate){
            yield $paginate;
        }
    }

    private function paginationPreset(Query $searchQuery, int $perPage): iterable
    {
        $totalCount = $this->countById();

        $pageNum = new \stdClass();

        $isAllItemsInLastPage = $totalCount % $perPage;
        0 === $isAllItemsInLastPage
            ? [$pageNum->isNotAllItemsInLastPage = false, $pageNum->count = $totalCount]
            : [$pageNum->isNotAllItemsInLastPage = true, $pageNum->count = $totalCount - 1];

        for ($page = 1; $page <= $pageNum->count; $page++) {
            if ($page === $pageNum->count && $pageNum->isNotAllItemsInLastPage === true) {
                $perPage += $isAllItemsInLastPage;
            }

            $query = $searchQuery
                ->setFirstResult($perPage * ($page - 1))
                ->setMaxResults($perPage);

            yield new Paginator($query);
        }
    }


    public function add(Participant $participant): void
    {
        $this->_em->persist($participant);
        $this->_em->flush();
    }

    public function addWithoutFlush(Participant $participant): void
    {
        $this->_em->persist($participant);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }
}