<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Participant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use function Zenstruck\Foundry\faker;

class ParticipantFixtures extends Fixture
{
    const DATA_COUNT = 33;

    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::DATA_COUNT; $i++) {
            $participant = (new Participant())
                ->setName(faker()->name,)
                ->setEmail(faker()->email)
                ->setCongratulated(false)
                ->setSanta(null);
            $manager->persist($participant);
        }
        $manager->flush();
    }
}