<?php

declare(strict_types=1);

namespace App\Command\Santa\SendEmails;

use App\Command\GiftEmail\Create\CreateGiftEmailCommand;
use App\Entity\Participant;
use App\Repository\ParticipantRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class SendEmailsCommandHandler
{
    public function __construct(
        private readonly ParticipantRepository $participantRepository,
        private readonly MessageBusInterface   $commandBus,
    )
    {
    }

    public function __invoke(SendEmailsCommand $command): void
    {
        $paginate = $this->participantRepository->getAllWithRelativePagination();

        foreach ($paginate as $participants) {
            $this->sendEmails($participants);
        }
    }

    private function sendEmails(iterable $participants): void
    {
        /** @var Participant $participant */
        foreach ($participants as $participant) {
            try {
                $this->commandBus->dispatch(new CreateGiftEmailCommand(
                    $participant->getId(),
                    $participant->getName(),
                    $participant->getEmail(),
                    $participant->getSanta()?->getEmail(),
                ));
            } catch (HandlerFailedException $e) {
            }
        }
    }
}