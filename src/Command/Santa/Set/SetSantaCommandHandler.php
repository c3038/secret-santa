<?php

declare(strict_types=1);

namespace App\Command\Santa\Set;

use App\Command\GiftEmail\Sent\SentGiftEmailCommand;
use App\Entity\Participant;
use App\Repository\ParticipantRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class SetSantaCommandHandler
{
    public function __construct(
        private readonly ParticipantRepository $participantRepository,
    )
    {
    }

    public function __invoke(SetSantaCommand $command): void
    {
        $paginate = $this->participantRepository->getAllWithSantaWithRelativePagination();

        foreach ($paginate as $participants) {
            $this->setSentData($participants);
        }
    }

    private function setSentData(iterable $data): void
    {
        $participants = [...$data];

        shuffle($participants);
        $totalParticipants = count($participants);

        for ($i = 0; $i < $totalParticipants; $i++) {
            /** @var Participant $santa */
            $santa = $participants[$i];
            /** @var Participant $recipient */
            $recipient = $participants[($i + 1) % $totalParticipants];
            $recipient->setSanta($santa);
            $this->participantRepository->addWithoutFlush($recipient);
        }
        $this->participantRepository->flush();
    }
}