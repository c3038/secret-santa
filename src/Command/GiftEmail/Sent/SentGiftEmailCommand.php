<?php

declare(strict_types=1);

namespace App\Command\GiftEmail\Sent;

class SentGiftEmailCommand
{
    public function __construct(
        readonly private int    $id,
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }
}