<?php

declare(strict_types=1);

namespace App\Command\GiftEmail\Sent;

use App\Command\GiftEmail\Create\CreateGiftEmailCommand;
use App\Dto\GiftSendDto;
use App\Repository\ParticipantRepository;
use App\Service\EmailService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class SentGiftEmailCommandHandler
{
    public function __construct(
        private readonly ParticipantRepository $participantRepository,
    )
    {
    }

    public function __invoke(SentGiftEmailCommand $command): void
    {
        $participant = $this->participantRepository->find($command->getId());
        $participant->setCongratulated(true);

        $this->participantRepository->add($participant);
    }
}