<?php

declare(strict_types=1);

namespace App\Command\GiftEmail\Create;

use App\Command\GiftEmail\Sent\SentGiftEmailCommand;
use App\Dto\GiftSendDto;
use App\Service\EmailService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
class CreateGiftEmailCommandHandler
{
    public function __construct(
        private readonly EmailService          $emailService,
        private readonly MessageBusInterface   $commandBus,
    )
    {
    }

    public function __invoke(CreateGiftEmailCommand $command): void
    {
        $dto = new GiftSendDto(
            $command->getName(),
            $command->getEmail(),
            $command->getSantaEmail()
        );
        $res = $this->emailService->sendEmail($dto);
        if($res) {
            $this->commandBus->dispatch(new SentGiftEmailCommand($command->getId()));
        }
    }
}