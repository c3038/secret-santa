<?php

declare(strict_types=1);

namespace App\Command\GiftEmail\Create;

class CreateGiftEmailCommand
{
    public function __construct(
        readonly private int    $id,
        readonly private string $name,
        readonly private string $email,
        readonly private string $santaEmail,
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getSantaEmail(): string
    {
        return $this->santaEmail;
    }


}