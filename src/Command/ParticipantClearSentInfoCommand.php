<?php

namespace App\Command;

use App\Entity\Participant;
use App\Repository\ParticipantRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'participant:clear-send',
    description: 'Clear info in db by send gifts',
)]
class ParticipantClearSentInfoCommand extends Command
{
    public function __construct(
        private readonly ParticipantRepository $participantRepository,
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $paginate = $this->participantRepository->getAllWithRelativePagination();

        foreach ($paginate as $participants) {
            $this->clearSentDataInfo($participants);
        }

        return Command::SUCCESS;
    }

    private function clearSentDataInfo(iterable $participants): void
    {
        foreach ($participants as $participant) {
            /** @var Participant $participant */
            $participant
                ->setSanta(null)
                ->setCongratulated(false);
            $this->participantRepository->add($participant);
        }
    }
}
