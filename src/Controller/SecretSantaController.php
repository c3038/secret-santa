<?php

namespace App\Controller;

use App\Service\SantaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SecretSantaController extends AbstractController
{
    public function __construct(
        private readonly SantaService $santaService,
    )
    {
    }

    #[Route('/secret/santa', name: 'set_secret_santa')]
    public function index(): Response
    {
        $this->santaService->assignSantas();

        return $this->redirectToRoute('participant_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/secret/santa/send', name: 'send_secret_santa')]
    public function send(): Response
    {
        $this->santaService->send();

        return $this->redirectToRoute('participant_index', [], Response::HTTP_SEE_OTHER);
    }
}
