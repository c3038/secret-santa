<?php

namespace App\Controller;

use App\Entity\Participant;
use App\Form\ParticipantType;
use App\Repository\ParticipantRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ParticipantController extends AbstractController
{
    public function __construct (
        private readonly ParticipantRepository $participantRepository,
        private readonly Participant $participant,
    )
    {
    }

    #[Route('participant/new', name: 'participant_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
        $form = $this->createForm(ParticipantType::class, $this->participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->participantRepository->add($this->participant);

            return $this->redirectToRoute('participant_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('participant/new.html.twig', [
            'participant' => $this->participant,
            'form' => $form,
        ]);
    }

    #[Route('participant/index', name: 'participant_index', methods: ['GET'])]
    public function index(PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $this->participantRepository->getAllWithSantaQuery(),
            $request->query->getInt('page', 1),
            4
        );

        return $this->render('participant/index.html.twig', [
            'participants' => $pagination,
        ]);
    }
}
